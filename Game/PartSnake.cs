﻿using System;
using System.Windows;

namespace SnakeWPFSample.Game
{
    // Отдельная часть змеи
    public class PartSnake
    {
        public UIElement UiElement { get; set; }
        public Point     Position  { get; set; }
        public bool      IsHead    { get; set; }
        
    }
}
﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace SnakeWPFSample
{
    public partial class ItemControlExample : Window
    {
        public ObservableCollection<Employee> List1
        {
            get; set;
        } 
        
        public ItemControlExample()
        {
            
            List1 = new ObservableCollection<Employee>();
            InitializeComponent();
            
//            List1.Add(new Employee(){Firstname = "Роман", Lastname = "Обромович"});
//            List1.Add(new Employee(){Firstname = "Евгений", Lastname = "Пушкин"});
//            List1.Add(new Employee(){Firstname = "Михаил", Lastname = "Ксенофонтов"});

            //this.Save();
            this.Load();
        }

        private void Save()
        {
            //throw new System.NotImplementedException();
            XmlSerializer ser = new  XmlSerializer(typeof(ObservableCollection<Employee>));
            using(StreamWriter output = new StreamWriter("list.txt"))
            {
                ser.Serialize(output, this.List1);
            }
        }

        private void Load()
        {
            XmlSerializer ser = new XmlSerializer(typeof(ObservableCollection<Employee>));

            using (Stream sr = new FileStream("list.txt", FileMode.Open))
            {
                ObservableCollection<Employee>List1 = (ObservableCollection<Employee>)ser.Deserialize(sr);
                foreach (Employee employee in List1)
                {
                    this.List1.Add(employee);
                }
            }
        }
    }

    public class Employee
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
﻿using System;
using System.Windows.Media;
using SnakeWPFSample.Game;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;
using Path = System.IO.Path;

namespace SnakeWPFSample
{
    public partial class MainWindow : INotifyPropertyChanged
    {
        private MediaPlayer player = new MediaPlayer();
        private int score;
        private int speed;
        public ObservableCollection<SnakeHighscore> HighscoreList { get; set; }

        public int Speed
        {
            get { return speed; }
            set
            {
                speed = value;
                this.OnPropertyChanged("Speed");
            }
        }

        public int Score
        {
            get { return score; }
            set
            {
                score = value;
                this.OnPropertyChanged("Score");
            }
        }

        public const int SquareSize = 20;
        public const int SnakeStartLength = 3;
        public const int SnakeStartSpeed = 100;
        public const int SnakeSpeedThreshold = 20;

        private SolidColorBrush ColorSnakeBody = Brushes.Beige;
        private SolidColorBrush ColorSnakeHead = Brushes.Pink;
        
        private List<PartSnake> SnakeParts = new List<PartSnake>();

        private Random rnd = new Random();

        private UIElement food = new Rectangle();
        private ImageBrush foodBruh;

        public enum SnakeDirection
        {
            Left,
            Right,
            Up,
            Down
        }

        private SnakeDirection snakeDirection = SnakeDirection.Right;
        private int SnakeLength;
        private System.Windows.Threading.DispatcherTimer GameTickTimer = new DispatcherTimer();

        public MainWindow()
        {
            try
            {
                this.HighscoreList = new ObservableCollection<SnakeHighscore>();
                InitializeComponent();
                this.DataContext = this;
                string path = Path.GetFullPath("Resources\\applered.png");

                Uri uri1 = new Uri("pack://application:,,,/Resources/applered.png");
                foodBruh = new ImageBrush()
                {
                    ImageSource = new BitmapImage(uri1)
                };
                GameTickTimer.Tick += GameTickTimer_Tick;

                path = Path.GetFullPath("Resources/sound.mp3");
                //Uri uri = new Uri("pack://application:,,,/Resources/sound.mp3");
                player.MediaEnded += RepeatAudio;
                this.player.Open(new Uri(path));
                this.player.Play();
                LoadHighscoreList();
                //this.HighscoreList.Add(new SnakeHighscore(){PlayerName = "Boo$$GYM",Score = 999});
                SaveHighscoreList();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_ContentRender(object sender, EventArgs e)
        {
            StartNewGame();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
//            string path = Path.GetFullPath("Resoures\\sound.mp3");
//            player.MediaFailed += media_MediaFailed;
//            Uri link = new Uri("Resoures\\sound.mp3");
//            player.Open(link);
//            player.Play();
        }

        private void StartNewGame()
        {
            this.brd_WelcomMessage.Visibility = Visibility.Visible;

            foreach (PartSnake snakeBodyPart in SnakeParts)
            {
                if (snakeBodyPart.UiElement != null)
                    can_GameField.Children.Remove(snakeBodyPart.UiElement);
            }

            SnakeParts.Clear();

            if (food != null)
                can_GameField.Children.Remove(food);

            Score = 0;
            SnakeLength = SnakeStartLength;
            snakeDirection = SnakeDirection.Right;
            SnakeParts.Add(new PartSnake() {Position = new Point(SquareSize * 5, SquareSize * 5)});

            DrawFood();
            DrawSnake();


            GameTickTimer.Interval = TimeSpan.FromMilliseconds(SnakeStartSpeed);
            UpdateGameStatus();
            //GameTickTimer.IsEnabled = true;
        }


        private void GameTickTimer_Tick(object sender, EventArgs e)
        {
            MoveSnake();
        }

        private void DrawSnake()
        {
            foreach (PartSnake part in SnakeParts)
            {
                if (part.UiElement == null)
                {
                    part.UiElement = new Rectangle()
                    {
                        Width = SquareSize,
                        Height = SquareSize,
                        //ColorSnakeHead
                        Fill =  part.IsHead ? ColorSnakeHead : ColorSnakeBody 
                    };
                    can_GameField.Children.Add(part.UiElement);
                    Canvas.SetTop(part.UiElement, part.Position.Y);
                    Canvas.SetLeft(part.UiElement, part.Position.X);
                }
            }
        }

        private void MoveSnake()
        {
            try
            {
                while (SnakeParts.Count >= SnakeLength)
                {
                    can_GameField.Children.Remove(SnakeParts[0].UiElement);
                    SnakeParts.RemoveAt(0);
                }

                foreach (PartSnake part in SnakeParts)
                {
                    (part.UiElement as Rectangle).Fill = ColorSnakeBody;
                    part.IsHead = false;
                }

                PartSnake oldHead = SnakeParts[SnakeParts.Count - 1];
                double nextX = oldHead.Position.X;
                double nextY = oldHead.Position.Y;

                switch (snakeDirection)
                {
                    case SnakeDirection.Left:
                        nextX -= SquareSize;
                        break;
                    case SnakeDirection.Right:
                        nextX += SquareSize;
                        break;
                    case SnakeDirection.Up:
                        nextY -= SquareSize;
                        break;
                    case SnakeDirection.Down:
                        nextY += SquareSize;
                        break;
                }

                SnakeParts.Add(new PartSnake()
                {
                    Position = new Point(nextX, nextY),
                    IsHead = true
                });

                DrawSnake();
                SnakeCheckCollision();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private Point GetNextFoodsPosition()
        {
            int maxX = (int) (can_GameField.ActualWidth / SquareSize);
            int maxY = (int) (can_GameField.ActualHeight / SquareSize);

            int foodX = rnd.Next(0, maxX) * SquareSize;
            int foodY = rnd.Next(0, maxY) * SquareSize;

            foreach (PartSnake part in SnakeParts)
            {
                if ((part.Position.X == foodX) && (part.Position.Y == foodY))
                {
                    return GetNextFoodsPosition();
                }
            }

            return new Point(foodX, foodY);
        }

        private void DrawFood()
        {
            try
            {
                Point foodPoint = this.GetNextFoodsPosition();
                this.food = new Rectangle()
                {
                    Width = SquareSize,
                    Height = SquareSize,
                    Fill = foodBruh
                };

                can_GameField.Children.Add(food);
                Canvas.SetLeft(food, foodPoint.X);
                Canvas.SetTop(food, foodPoint.Y);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            SnakeDirection currentDirection = snakeDirection;

            switch (e.Key)
            {
                case Key.Up:
                    snakeDirection = snakeDirection == SnakeDirection.Down ? currentDirection : SnakeDirection.Up;
                    MoveSnake();
                    break;
                case Key.Down:
                    snakeDirection = snakeDirection == SnakeDirection.Up ? currentDirection : SnakeDirection.Down;
                    MoveSnake();
                    break;
                case Key.Right:
                    snakeDirection = snakeDirection == SnakeDirection.Left ? currentDirection : SnakeDirection.Right;
                    MoveSnake();
                    break;
                case Key.Left:
                    snakeDirection = snakeDirection == SnakeDirection.Right ? currentDirection : SnakeDirection.Left;
                    MoveSnake();
                    break;
                case Key.Space:
                    
                    if( brd_NewHighscore.Visibility == Visibility.Collapsed )
                    {
                        this.GameTickTimer.IsEnabled = !this.GameTickTimer.IsEnabled;
                        
                        if (brd_WelcomMessage.Visibility == Visibility.Visible)
                        {
                            brd_WelcomMessage.Visibility = Visibility.Hidden;
                        }
                        else if (brd_HighScoreList.Visibility == Visibility.Visible)
                        {
                            brd_HighScoreList.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            brd_WelcomMessage.Visibility = Visibility.Visible;
                        }
                    }

                    break;
            }
        }

        private void SnakeCheckCollision()
        {
            PartSnake head = SnakeParts[SnakeParts.Count - 1];
            Point position = head.Position;

            if (position.X == Canvas.GetLeft(food) && position.Y == Canvas.GetTop(food))
            {
                EatFood();
                return;
            }

            if (
                position.X < 0 ||
                position.X >= can_GameField.ActualWidth ||
                position.Y < 0 ||
                position.Y >= can_GameField.ActualHeight
            )
            {
                EndGame();
                return;
            }

            foreach (PartSnake currentPart in SnakeParts.Where(p => p.IsHead == false))
            {
                if (currentPart.Position.X == position.X && currentPart.Position.Y == position.Y)
                {
                    EndGame();
                    return;
                }
            }

        }

        private void EndGame()
        {
            GameTickTimer.IsEnabled = false;
            //MessageBox.Show("Вы проиграли!!!(С подливой)\n\nЧтобы начать новую игру нажмите пробел...", "Конец!");
            
            if( HighscoreList.Count == 0 || HighscoreList.Max( r => r.Score) < Score)
                brd_NewHighscore.Visibility = Visibility.Visible;
            else
            {
                MessageBoxResult result = MessageBox.Show("Неудача, попробовать снова?", "Вы умерли!", MessageBoxButton.YesNo, MessageBoxImage.Hand);

                switch (result)
                {
                    case MessageBoxResult.No : 
                        Application.Current.Shutdown();
                        break;
                    case MessageBoxResult.Yes:
                        StartNewGame();
                        break;
                }
            }
            
        }

        private void EatFood()
        {
            SnakeLength++;
            Score++;

            GameTickTimer.Interval =
                TimeSpan.FromMilliseconds(Math.Max(SnakeSpeedThreshold, SnakeStartSpeed - Score * 2));
            can_GameField.Children.Remove(food);

            DrawFood();
            UpdateGameStatus();
        }

        private void UpdateGameStatus()
        {
            Speed = (int) (SnakeStartSpeed - GameTickTimer.Interval.TotalMilliseconds);
//            this.Title = String.Format("SnakeWPF - Score: {0} - Game speed: {1}", 
//                Score,
//                SnakeStartSpeed - GameTickTimer.Interval.TotalMilliseconds);
        }

        private void Window_MouseDown(object sender, EventArgs e)
        {
            this.DragMove();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void CloseGame(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Pause_Click(object sender, EventArgs e)
        {
            this.GameTickTimer.IsEnabled = !this.GameTickTimer.IsEnabled;
        }

        private void BtnAddToHighscoreList_Click(object sender, RoutedEventArgs e)
        {
            this.HighscoreList.Add(new SnakeHighscore()
            {
                PlayerName = txt_PalyerName.Text, 
                Score = this.score
            });
            this.SaveHighscoreList();
            this.brd_NewHighscore.Visibility = Visibility.Collapsed;
            //this.brd_HighScoreList.Visibility = Visibility.Visible;
            StartNewGame();
        }

        private void BtnShowHighscoreList_Click(object sender, RoutedEventArgs e)
        {
            brd_WelcomMessage.Visibility = Visibility.Hidden;
            brd_HighScoreList.Visibility = Visibility.Visible;
        }

        private void LoadHighscoreList()
        {
            if (File.Exists("snake_highscorelist.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<SnakeHighscore>));
                using (Stream reader = new FileStream("snake_highscorelist.xml", FileMode.Open))
                {
                    List<SnakeHighscore> tempList = (List<SnakeHighscore>) serializer.Deserialize(reader);
                    this.HighscoreList.Clear();
                    int i = 0;
                    int max = 5;
                    foreach (var item in tempList.OrderByDescending(x => x.Score))
                    {
                        if (i == max) break;
                        this.HighscoreList.Add(item);
                        i++;
                    }
                }
            }
        }

        private void SaveHighscoreList()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<SnakeHighscore>));
            using (Stream writer = new FileStream("snake_highscorelist.xml", FileMode.Create))
            {
                serializer.Serialize(writer, this.HighscoreList);
            }
        }


        private void RepeatAudio(object sender, EventArgs e)
        {
            player.Position = TimeSpan.Zero;
            player.Play();
        }
    }
}
﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SnakeWPFSample
{
    public partial class TimerExam : Window
    {
        DispatcherTimer dt = new DispatcherTimer();
        
        public TimerExam()
        {
            InitializeComponent();
            dt.Interval = TimeSpan.FromMilliseconds(75);
            dt.Tick += MoveRect;
            dt.IsEnabled = true;
        }

        public void MoveRect(object sender, EventArgs e)
        {
            double top = Canvas.GetTop(erect);
            double left = Canvas.GetLeft(erect);
            if ( top >= (field.ActualHeight - erect.Height - 2)  ||  left >= (field.ActualWidth- erect.Width -2))
                dt.IsEnabled = false;
            
            Canvas.SetTop(erect, top + 2);
            Canvas.SetLeft(erect, left + 2);
            
        
        }


        private void TimerExam_OnContentRendered(object sender, EventArgs e)
        {
            
        }
    }
}